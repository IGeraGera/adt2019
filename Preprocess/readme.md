## Import Annotations, Preproccess Audio, Transform Features (for RNN)

![Block diagram : Import, Preprocess and Transform Training and Testing Dataset](images/overall_system.png)

+ **data.py**	: Contains a class to batch import anotation and audio files from a directory. We also provide methods for RNN feature transformation
	+ **annotation.py**	: Contains a class to import one annotation file
	+ **audio.py** 	: Contains a class to import and preprocess audio


**Import_Training-Testing_dataset.py** is the controller script of data class (data.py). 
After running the controller script whe get the saved dataset files :

+ X_train = logarithmic spectogram for audio files **#train.wav
+ y_train = annotations for audio files **#train.wav
+ X_test = logarithmic spectogram for audio files **#MIX.wav
+ y_test = annotations for audio files **#MIX.wav
