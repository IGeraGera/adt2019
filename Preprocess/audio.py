import madmom
from functools import partial
import matplotlib.pyplot as plt


# Class for importing and pre - processing a single audio file
class Logarithmic_Spectogram_Processor(madmom.processors.SequentialProcessor):
    def __init__(self, fmin=20.0, fmax=20000.0, num_bands=12):
        # Processing Chain
        # Import Audio Processor
        load_wav = madmom.audio.signal.SignalProcessor()
        # Normalize Audio Processor
        normalize_wav = partial(madmom.audio.signal.normalize)
        # Calculate Logarithmic Filtered Spectrogram Processor
        compute_logarithmic_spectogram = madmom.audio.spectrogram.LogarithmicFilteredSpectrogramProcessor(
            fmin=fmin, fmax=fmax, num_bands=num_bands)
        super(Logarithmic_Spectogram_Processor, self).__init__(
            [load_wav, normalize_wav, compute_logarithmic_spectogram])


# Class for importing and pre - processing a single audio file
class Spectral_Flux_Processor(madmom.processors.SequentialProcessor):
    def __init__(self, fmin=20.0, fmax=20000.0, num_bands=12):
        # Processing Chain
        # Import Audio Processor
        load_wav = madmom.audio.signal.SignalProcessor()
        # Normalize Audio Processor
        normalize_wav = partial(madmom.audio.signal.normalize)
        # Calculate Logarithmic Filtered Spectrogram Processor
        compute_logarithmic_spectogram = madmom.audio.spectrogram.LogarithmicFilteredSpectrogramProcessor(
            fmin=fmin, fmax=fmax, num_bands=num_bands)
        # First_order_differences Preprocessor
        first_order_dif = madmom.audio.SpectrogramDifferenceProcessor(
            diff_max_bins=3, positive_diffs=True)
        super(Spectral_Flux_Processor, self).__init__([
            load_wav, normalize_wav, compute_logarithmic_spectogram,
            first_order_dif
        ])
