from data import data
import matplotlib.pyplot as plt
from numpy import save
from keras.utils import to_categorical
import numpy as np
# This is the main script for importing, preprocessing and transforming
# annotations as well as audio for the training and testing process.
# The following help functions are IMPORTANT for this script to work (in tree order)
# - data.py
#       * annotation.py
#       * audio.py
# More info : readme.md

if __name__ == '__main__':
    # Importing & loading IDMT annotation - audio for RNN training
    Training_Dataset = data('dataset/IDMT/annotation_svl/','dataset/IDMT/audio/')

    # Transform features for RNN input - output
    RNN_input_features, RNN_output_annotations, Eval_Annotations_Matrix = data.feature_transformation(
        Training_Dataset)

    # Saving Training data as .npy files
    save('X_train.npy', RNN_input_features)
    save('y_train.npy', RNN_output_annotations)
    save('Eval_annotations_matrix_train.npy',Eval_Annotations_Matrix)

    # Importing & loading IDMT annotation - audio for RNN testing
    Testing_Dataset = data('dataset/IDMT/annotation_xml/', 'dataset/IDMT/audio/')
    # Transform features for RNN input - output
    RNN_input_features, RNN_output_annotations, Eval_Annotations_Matrix = data.feature_transformation(
        Testing_Dataset)

    # Saving Testing data as .npy files
    save('X_test.npy', RNN_input_features)
    save('y_test.npy', RNN_output_annotations)
    save('Eval_annotations_matrix_test.npy',Eval_Annotations_Matrix)
