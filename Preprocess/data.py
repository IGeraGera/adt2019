import annotation
import audio
import os
from keras.utils import to_categorical
import numpy as np
import librosa
import math


# Class to batch import annotations and audio files
class data:
    # Data class needs to be Initialized with annotations and audio path
    def __init__(self, annotations_data_path, audio_data_path):
        self.annotations = []
        self.audio_files_logarithmic_spectogram = []
        self.audio_files_spectral_flux = []
        if os.path.exists(annotations_data_path) and os.path.exists(
                audio_data_path):
            self.import_from(annotations_data_path, audio_data_path)
        else:
            print("Warning : Anotations or audio path doesn't exists.")
            print("Empty data object created.\n")

    def import_from(self, annotation_data_path, audio_data_path):
        # Checking if it's a directory OR file
        if annotation_data_path.endswith('/'):
            print("Importing annotation(s) from : " + annotation_data_path)
            print("Importing audio file(s) from : " + audio_data_path)
            # Import Files in Directory
            i = 1
            for annotation_file_name in os.listdir(annotation_data_path):
                # Make full_annotation file_path
                full_annotation_file_path = os.path.join(
                    annotation_data_path, annotation_file_name)
                # Make audio full audio file path
                full_audio_file_path = os.path.join(audio_data_path,annotation.audio_file_name)
                # If both annotation and audio files exist then import them
                if os.path.exists(full_annotation_file_path) and os.path.exists(full_audio_file_path):
                    # Load annotation
                    annotation = self.import_anotation_from(
                        full_annotation_file_path)
                    # Print Progress Bar
                    data.__printProgressBar(i,
                                            len(os.listdir(annotation_data_path)),
                                            annotation_file_name[:-4], 'Complete')
                    
                    # Load and process audio file
                    self.import_audio_from(full_audio_file_path)
                    # Delete annotation
                    annotation.__del__()
                    i = i + 1
            print(len(os.listdir(annotation_data_path)),
                  "annotation & audio files imported")
        else:
            # One File was given
            print("Importing annotation file from : " + annotation_data_path)
            print("Importing audio file from : " + audio_data_path)
            full_annotation_file_path = annotation_data_path
            full_audio_file_path = audio_data_path
            annotation = self.import_anotation_from(full_annotation_file_path)
            # Make audio file path
            full_audio_file_path = os.path.join(audio_data_path,
                                                annotation.audio_file_name)
            # Delete annotation
            annotation.__del__()
            # Load and pre process audio
            self.import_audio_from(full_audio_file_path)

    # Method to import a single anotation file
    def import_anotation_from(self, annotation_file_path):
        annotation_file = annotation.annotation.import_from_file(
            annotation_file_path)
        self.annotations.append(annotation_file)
        return annotation_file

    # Method to import and process a single audio file
    def import_audio_from(self, full_audio_file_path):
        # Calculate Logarithmic Spectogram With Processor
        audio_log_spec = audio.Logarithmic_Spectogram_Processor().process(
            full_audio_file_path)
        # Append Logarithmic Spectogram
        self.audio_files_logarithmic_spectogram.append(audio_log_spec)
        # Calculate spectral flux
        spectral_flux = audio.Spectral_Flux_Processor().process(
            full_audio_file_path)
        # Append spectral flux
        self.audio_files_spectral_flux.append(spectral_flux)

    # Feature Transformation Method
    # In this Function we Transform the annotated data following these steps :
    # 1. Find samples from onset seconds
    # 2. Transform instruments to one_hot_encoding
    # 3. Expand 1 & 2 lists by linking annotation moments (samples) to logarithmic frames
    def feature_transformation(self):
        # Initialize anotation linked to frames for one file
        one_hot_encoding_annotations_to_frames = []
        # Initialize RNNinput
        RNNinput = []
        # Initialize onset_sec_samples for Evaluation
        annotations = []
        # Iterate over tracks
        for i in range(0, len(self.annotations)):
            # Keeping the number of windows of logatithmic spectogram of each track
            number_of_windows = self.audio_files_logarithmic_spectogram[
                i].shape[0]
            # 2. Transform instruments code to one_hot_encoding
            code_to_one_hot = to_categorical(
                self.annotations[i].onset_instrument_code, num_classes=3)

            # 3. Now we need to link annotation moments to frames
            # Initialize list with linked annotations to frames
            annotations_linked_to_frames = np.zeros((number_of_windows, 3),
                                                    dtype=float)

            # Find corresponding time - frame frame_indexes
            frame_indexes = librosa.time_to_frames(
                self.annotations[i].onset_sec,
                sr=44100,
                hop_length=441,
                )
            # Make frame_indexes start from zero
            if (frame_indexes[frame_indexes < 0].shape[0] > 0):
                frame_indexes[frame_indexes < 0] += -min(
                    frame_indexes[frame_indexes < 0])
            for j in range(frame_indexes.shape[0]):
                annotations_linked_to_frames[frame_indexes[j]] = np.logical_or(
                    annotations_linked_to_frames[frame_indexes[j]],
                    code_to_one_hot[j])
            one_hot_encoding_annotations_to_frames.append(
                annotations_linked_to_frames)

            # Contatate logarithmic spectogram with spectral_flux and make RNNinput
            spectral_flux = np.array(self.audio_files_spectral_flux[i])
            log_spec = np.array(self.audio_files_logarithmic_spectogram[i])
            RNNinput.append(np.concatenate((spectral_flux, log_spec), axis=1))

            # Make Evaluation matrix with onsets code, onsets seconds and onsets samples
            a = np.array(self.annotations[i].onset_instrument_code)
            b = np.array(self.annotations[i].onset_sec)
            c = np.concatenate((a, b))
            c = np.reshape(c, (2, a.shape[0]))
            annotations.append(c.T)

        #Finaly return features
        return np.array(RNNinput), np.array(
            one_hot_encoding_annotations_to_frames), np.array(annotations)

    # Method for Printing Progress
    def __printProgressBar(iteration,
                           total,
                           prefix='',
                           suffix='',
                           decimals=1,
                           length=100,
                           fill='█',
                           printEnd="\r"):
        """
        Call in a loop to create terminal progress bar
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
            printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
        """
        percent = ("{0:." + str(decimals) + "f}").format(
            100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print('\r%s  %s %s%% %s' % (prefix, bar, percent, suffix),
              end=printEnd)
        # Print New Line on Complete
        if iteration == total:
            print()
