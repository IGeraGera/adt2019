import os
from xml.dom import minidom
import numpy as np


# Class to hold one annotation data
class annotation:
    # Instrument Dictionary to map onset_instrument_label to onset_instrument_code
    instrument_dictionary = {"KD": 0, "SD": 1, "HH": 2}

    def __init__(self,
                 audio_file_name="",
                 onset_sec=[],
                 onset_instrument_label=[],
                 onset_instrument_code=[],
                 annotation_file_path="",
                 onset_sec_sample=[]):
        # Audio of annotated filename
        self.audio_file_name = audio_file_name
        # Onsets in time (seconds)
        self.onset_sec = np.array(onset_sec, dtype=np.float)
        # Instrument type onsets
        # **_label  : holds instrument label e.g. "SD"
        # **_code   : holds instrument coding according to instrument_dictionary,
        #             following the previous example :
        #             onset_instrument_label = "SD" whe have onset_instrument_label = 1
        self.onset_instrument_label = onset_instrument_label
        self.onset_instrument_code = np.array(onset_instrument_code)
        # Information regarding file paths of annotated audio and file path of annotation file
        self.annotation_file_path = annotation_file_path
        self.onset_sec_sample= onset_sec_sample 

    # Destructor
    def __del__(self):
        pass

    @classmethod
    def import_from_file(cls, annotation_file_path):
        # Get annotation file_name
        annotation_file_name = (os.path.basename(annotation_file_path))
        # Check the annotation format and call the right parser
        if annotation_file_name.endswith('.xml'):
            audio_file_name, onset_sec, onset_instrument_label, onset_instrument_code = cls.xml_parse(
                annotation_file_path)
        if annotation_file_name.endswith('.svl'):
            audio_file_name, onset_sec, onset_instrument_label, onset_instrument_code = cls.svl_parse(
                annotation_file_path)
        # Make annotation object with parsing data
        return cls(audio_file_name, onset_sec, onset_instrument_label,
                   onset_instrument_code, annotation_file_path, [])

    # Method for parsing .xml file
    @classmethod
    def xml_parse(cls, annotation_file_path):
        # Open file from path
        file = minidom.parse(annotation_file_path)
        # Parse filename of data sample
        name = file.getElementsByTagName("audioFileName")[0]
        audio_file_name = name.firstChild.data
        # Initialize
        onset_sec = []
        instruments_label = []
        # Classify data sample by events tag
        events = file.getElementsByTagName("event")
        for event in events:
            # Read Individual Onsets & Instruments from data sample
            onset_event = event.getElementsByTagName("onsetSec")[0]
            instruments_event = event.getElementsByTagName("instrument")[0]
            onset_sec.append(onset_event.firstChild.data)
            instruments_label.append(instruments_event.firstChild.data)
        # Find onset_instrument_code
        onset_instruments_code = []
        for i in range(0, len(instruments_label)):
            onset_instruments_code.append(
                cls.instrument_dictionary[instruments_label[i]])
        # Return values
        return audio_file_name,onset_sec,instruments_label,onset_instruments_code

    # Method for parsing .svl file
    @classmethod
    def svl_parse(cls, annotation_file_path):
        # Open file of data sample
        file = minidom.parse(annotation_file_path)
        # Make audio_file_name of the annotated data
        audio_file_name = (os.path.basename(annotation_file_path))
        audio_file_name = audio_file_name[:-4] + '.wav'
        if audio_file_name.endswith('#HH.wav'):
            instrument = "HH"
            onset_instrument_code = [cls.instrument_dictionary[instrument]]
        elif audio_file_name.endswith('#KD.wav'):
            instrument = 'KD'
            onset_instrument_code = [cls.instrument_dictionary[instrument]]
        else:
            instrument = 'SD'
            onset_instrument_code = [cls.instrument_dictionary[instrument]]

        ## Keep only the data-tagged field
        dataSVL = file.getElementsByTagName('data')[0]
        ## XML structure for the parameters:
        parametersSVL = dataSVL.getElementsByTagName('model')[0]
        ## XML structure for the dataset field, containing the points:
        datasetXML = dataSVL.getElementsByTagName('dataset')[0]
        ## XML structure with all the points from datasetXML:
        pointsXML = datasetXML.getElementsByTagName('point')
        ## converting to a somewhat easier to manipulate format:
        ## Dictionary for the parameters:
        parameters = {}
        for key in parametersSVL.attributes.keys():
            parameters[key] = parametersSVL.getAttribute(key)
        ## number of points (or regions):
        nbPoints = len(pointsXML)
        ## Initialize the numpy arrays (frame time stamps and durations):
        onset_sec = np.zeros([nbPoints], dtype=np.float)
        onset_sample = np.zeros([nbPoints], dtype=np.float)
        ## Iteration over the points:
        for node in range(nbPoints):
            ## converting sample to seconds for the time stamps and the durations:
            onset_sec[node] = np.double(
                pointsXML[node].getAttribute('frame')) / np.double(
                    parameters['sampleRate'])
            onset_sample[node] = np.double(
                pointsXML[node].getAttribute('frame'))
        # Return Results
        return audio_file_name, onset_sec, instrument, onset_instrument_code*nbPoints
