import numpy as np
import madmom


# Calculate TruePositives, FalsePositives and FalseNegatives detections for ONE track.
def Calculate_TP_FP_FN_Detections(detections, annotations, TOLERANCE_WINDOW):
    # Initialize TP_track, FP_track, FN_track
    TP_track = []
    FP_track = []
    FN_track = []

    # Find detections & annotations for each instrument
    # KD detections & annotations
    KD_detections = detections[detections[:, 0] == 0]
    KD_annnotations = annotations[annotations[:, 0] == 0]
    # SD detections & annotations
    SD_detections = detections[detections[:, 0] == 1]
    SD_annnotations = annotations[annotations[:, 0] == 1]
    # HH detections & annotations
    HH_detections = detections[detections[:, 0] == 2]
    HH_annnotations = annotations[annotations[:, 0] == 2]

    # Calculate TruePositives, FalsePositives and FalseNegatives detections for KD instrument with label 0.
    TP_KD, FP_KD, tn, FN_KD, error = madmom.evaluation.onsets.onset_evaluation(
        KD_detections[:, 1], KD_annnotations[:, 1], window=TOLERANCE_WINDOW)
    # Append Results
    TP_track.append(TP_KD)
    FP_track.append(FP_KD)
    FN_track.append(FN_KD)
    # Meter list
    KD = [TP_KD.shape[0], FP_KD.shape[0], FN_KD.shape[0]]

    # Calculate TruePositives, FalsePositives and FalseNegatives detections for SD instrument with label 1.
    TP_SD, FP_SD, tn, FN_SD, error = madmom.evaluation.onsets.onset_evaluation(
        SD_detections[:, 1], SD_annnotations[:, 1], window=TOLERANCE_WINDOW)
    # Append Results
    TP_track.append(TP_SD)
    FP_track.append(FP_SD)
    FN_track.append(FN_SD)
    # Meter list
    SD = [TP_SD.shape[0], FP_SD.shape[0], FN_SD.shape[0]]

    # Calculate TruePositives, FalsePositives and FalseNegatives detections for HH instrument with label 2.
    TP_HH, FP_HH, tn, FN_HH, error = madmom.evaluation.onsets.onset_evaluation(
        HH_detections[:, 1], HH_annnotations[:, 1], window=TOLERANCE_WINDOW)
    # Append Results
    TP_track.append(TP_HH)
    FP_track.append(FP_HH)
    FN_track.append(FN_HH)
    # Meter list
    HH = [TP_HH.shape[0], FP_HH.shape[0], FN_HH.shape[0]]

    # Return Results for on track
    return TP_track, FP_track, FN_track, np.array(KD), np.array(SD), np.array(
        HH)


# MAIN FUNCTION
# In this script we evaluate the RNN detections
# The detections and annotations are in the following format :
# Label Sec   
# 0     0.125	
# 2     0.125	
# 2     0.250	

if __name__ == '__main__':
    # Load annotations and detections
    annotations = np.load('evaluation_matrix_test.npy', allow_pickle=True)
    detections = np.load('detections_matrix.npy', allow_pickle=True)

    # Tolerace window is 50 ms
    TOLERANCE_WINDOW = 0.05

    # Initialize TP, FP and FN detections lists for all testing dataset
    TP_detections = []
    FP_detections = []
    FN_detections = []

    # Initialize TP, FP and FN meter for all instruments
    # It will be in the following format :
    # <instr> = [TP_<instr>, FP_<instr>, FN_<instr>].
    # E.g. for the KD instrument we will have
    # KD = [TP_KD, FP_KD, FN_KD]
    KD = np.array([0, 0, 0])
    SD = np.array([0, 0, 0])
    HH = np.array([0, 0, 0])

    # Calculate TruePositives, FalsePositives and FalseNegatives detections iteratively for each track.
    for track in range(0, annotations.shape[0]):
        # Get TP, FP, FN for each instrument in the track
        TP_track, FP_track, FN_track, KD_track, SD_track, HH_track = Calculate_TP_FP_FN_Detections(
            detections[track], annotations[track], TOLERANCE_WINDOW)
        # Append results
        # Detections
        TP_detections.append(TP_track)
        FP_detections.append(FP_track)
        FN_detections.append(FN_track)
        # Meters
        KD = KD + KD_track
        SD = SD + SD_track
        HH = HH + HH_track

    # PRINT RESULTS
    # Calculate and print evaluation for each instrument Class
    KD_Evaluation = madmom.evaluation.SimpleEvaluation(
        num_tp=KD[0],
        num_fp=KD[1],
        num_tn=0,
        num_fn=KD[2],
        name="Evaluation Of Kick Drum Class (KD) : ")
    print(KD_Evaluation)

    SD_Evaluation = madmom.evaluation.SimpleEvaluation(
        num_tp=SD[0],
        num_fp=SD[1],
        num_tn=0,
        num_fn=SD[2],
        name="Evaluation Of Snare Drum Class (SD) : ")
    print(SD_Evaluation)

    HH_Evaluation = madmom.evaluation.SimpleEvaluation(
        num_tp=HH[0],
        num_fp=HH[1],
        num_tn=0,
        num_fn=HH[2],
        name="Evaluation Of Hi - Hat Drum Class (HH) : ")
    print(HH_Evaluation)

    # Calculate and print the mean of previous evaluation metrics
    Mean_Evaluation = madmom.evaluation.MeanEvaluation(
        [KD_Evaluation, SD_Evaluation, HH_Evaluation], name='Mean Metrics')
    print('MEAN EVALUATION METRICS :\n', Mean_Evaluation)

    # Sum TP, FP and FN and then Calculate evalutation metric
    Overall_Evaluation = madmom.evaluation.SimpleEvaluation(
        num_tp=KD[0] + SD[0] + HH[0],
        num_fp=KD[1] + SD[1] + HH[1],
        num_tn=0,
        num_fn=KD[2] + SD[2] + HH[2],
        name="OVERALL EVALUATION METRICS : ")
    print(Overall_Evaluation)
