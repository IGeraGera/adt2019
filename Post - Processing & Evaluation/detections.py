import madmom
import numpy as np
import librosa
#Function that takes argument frame_index_list in form list[]=[np_for_KD[],np_for_SD[],np_for_HH[] ] 
#These np_for_@@ contain indexes of frames (for 1 track) after peak- peaking for every instrument
def detections(frame_index_list):
    #Loading  the numpy arrays that contain the  frames after peak peaking on @@_frame_index for each instrument
    KD_frame_index, SD_frame_index,HH_frame_index = frame_index_list[0],frame_index_list[1],frame_index_list[2]
    #add [-1 -1] added to detections only to resolve implemention issue on concatanate(just techinal )
    detections=np.empty((1,2))
    #In this stage check first if there  the numpy arrays for its instrument is't not empty 
    if(KD_frame_index.shape[0]!=0):
        d=jo(KD_frame_index)
        #initialize a np array with that has lenght of each instrument after passed on jo function
        inst_KD=np.full(d.shape[0],0)
        #concatenate the 2 arrays in form
        #   0   0.804
        #   0   0.854
        #   0   0.879
        #   0   0.880
        KD_Detections=np.concatenate((inst_KD,d),axis=0)
        KD_Detections=KD_Detections.reshape(2,d.shape[0])
        KD_Detections=KD_Detections.T
        #Concatenate the numpy array with the array for each instrument to the detections numpy array for that track
        detections=np.concatenate((detections,KD_Detections),axis=0)
    if(SD_frame_index.shape[0]!=0):
        d=jo(SD_frame_index)
        inst_SD=np.full(d.shape[0],1)
        SD_Detections=np.concatenate((inst_SD,d),axis=0)
        SD_Detections=SD_Detections.reshape(2,d.shape[0])
        SD_Detections=SD_Detections.T
        detections=np.concatenate((detections,SD_Detections),axis=0)
    if(HH_frame_index.shape[0]!=0):
        d=jo(HH_frame_index)
        inst_HH=np.full(d.shape[0],2)
        HH_Detections=np.concatenate((inst_HH,d),axis=0)
        HH_Detections=HH_Detections.reshape(2,d.shape[0])
        HH_Detections=HH_Detections.T
        detections=np.concatenate((detections,HH_Detections),axis=0)
    detections=detections[1:]
    
    #Finally aranging the detection array  by order of time on seconds 

    detections=detections[np.argsort(detections[:,1])]
    #final form 0   0.852
    #           1   0.879
    #           2   0.899
    #           0   0.921
    #           2   0.951
    return detections

#jo function takes @@_frame_index numpy array and returns a numpy array that contains seconds
def jo(frame_indexes):
    #First transforming the numpy array from frame indexs to seconds
    #and takes the midle (1024=2048/2)
    seconds = librosa.core.frames_to_time(frame_indexes, sr=44100.0, hop_length=441)
    #Second,checking if the time between two neighborhood is less than 20ms 
    #and if it is then takes only the first time 
    onset_error_in_seconds = np.diff(seconds,prepend=seconds[0])
    onset_seconds=seconds[onset_error_in_seconds>0.02]
    return onset_seconds 

# MAIN FUNCTION 
if __name__ == '__main__':
    # AF_RNN is the output of the RNN activation function of all inputs
    # it will be in the following format :
    # [[[np.array(KD_AF)],[np.array(SD_AF)],[np.array(HH_AF)]],
    # [[np.array(KD_AF)],[np.array(SD_AF)],[np.array(HH_AF)]] 
    # * - * - * - * - * - * - * - * - * - * - * - * - * - * - *]
    AF_RNN = np.load('y_pred.npy', allow_pickle=True) #import predicted values 
    # Threshold for peak - picking. This can be changed 
    KD_THRESHOLD, SD_THRESHOLD, HH_THRESHOLD = 0.4 , 0.4 , 0.4
    # Annotation detections list to be returned
    tot_detections=[]
    # Calculate annotation detections iteratively for each track.
    for track in range(0,AF_RNN.shape[0]):
        # Initialize peak_picking_indexes list
        peak_picking_indexes=[]
        # Get track activation functions
        track_AF = AF_RNN[track]

        # Apply Peak - Picking Algorithm
        # Append peak - picking for KD instrument track_AF[0]
        peak_picking_indexes.append(madmom.features.onsets.peak_picking(track_AF[:,0], threshold=KD_THRESHOLD, smooth=None, pre_avg=0, post_avg=0, pre_max=1, post_max=1))
        # Append peak - picking for SD instrument track_AF[1]
        print(track_AF[:,0])
        peak_picking_indexes.append(madmom.features.onsets.peak_picking(track_AF[:,1], threshold=SD_THRESHOLD, smooth=None, pre_avg=0, post_avg=0, pre_max=1, post_max=1))
        # Append peak - picking for HH instrument track_AF[2]
        peak_picking_indexes.append(madmom.features.onsets.peak_picking(track_AF[:,2], threshold=HH_THRESHOLD, smooth=None, pre_avg=0, post_avg=0, pre_max=1, post_max=1))
        
        # Construct onset detections for each track
        # peak_picking_indexes contains the output (frame indices) of the peak - picking algorithm
        # it will be in the following format : 
        # [[np.array(KD_indexes)],[np.array(SD_indexes)],[np.array(HH_indexes)]]
        detections_for_one_track = detections(peak_picking_indexes)
        # Append result to list
        tot_detections.append(detections_for_one_track) 

    np.save('detections_matrix.npy', np.array(tot_detections))
    print('Saved Detections')
